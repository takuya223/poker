package sample;

import java.util.ArrayList;
import java.util.List;

public class Card {

	protected static int MAX_CARD_SUIT_NUMBER = 13;
	protected static int MAX_SUIT_NUMBER = 4;
	protected static String[] SUIT = { "H", "S", "D", "C", };
	protected List<String> all_card;
	protected int card_count;
	protected int jokerNumber;
	protected int maxCardNumber;

	public Card(int... jokerCount) {
		// JOKERを設定する
		jokerNumber = 0;
		if (jokerCount.length > 0) {
			jokerNumber = jokerCount[0];
		}
		// 全トランプ枚数を設定する
		maxCardNumber = MAX_CARD_SUIT_NUMBER * MAX_SUIT_NUMBER + jokerNumber;

		initCard();
	}

	private void initCard() {
		// 全トランプを設定する
		all_card = new ArrayList<>();
		for (int i = 0; i < MAX_SUIT_NUMBER; i++) {
			for (int j = 0; j < MAX_CARD_SUIT_NUMBER; j++) {
				all_card.add(SUIT[i] + String.valueOf(j + 1));
			}
		}

		for (int i = 0; i < jokerNumber; i++) {
			all_card.add("J");
		}
		card_count = maxCardNumber;

	}

	public void resetCard() {
		initCard();
	}

	public String getRundumCsvStr(int number) {
		String ret = "";

		for (int i = 0; i < number; i++) {
			if (card_count <= 0) {
				return ret;
			}
			int r = (int) (Math.random() * 100) % card_count;

			if (i > 0) {
				ret = ret + ",";
			}
			ret = ret + all_card.get(r);
			all_card.remove(r);
			card_count--;

		}
		return ret;
	}

}
