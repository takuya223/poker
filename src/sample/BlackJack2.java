package sample;

/**
 *
 * @author ekatata
 *
 */
public class BlackJack2 {

	/**
	 *
	 * @param currentHand
	 * @return
	 */
	public String getRank(String currentHand) {
		System.out.println(currentHand);

		// 点数判定
		int nPoint = getPoint(currentHand);

		// 結果文字列作成
		String result = getRankStr(nPoint);

		return result;
	}

	// BJの合計点を返す
	private int getPoint(String hand) {
		// カード分割
		String[] cards = hand.split(",", 0);

		int total = 0;
		// カードの枚数分ループする
		int exist1 = 0;
		for (int i = 0; i < cards.length; i++) {
			// カード分析
			int p = Integer.parseInt(cards[i].substring(1, cards[i].length()));
			// 点数計算
			if (p == 1) {
				exist1++;
				total += 1;
			} else if (p <= 10) {
				total += p;
			} else {
				total += 10;
			}
		}
		if (total <= 11 && exist1 > 0) {
			total += 10;
		}

		// 合計点を返す
		return total;
	}

	// 合計点に対応する文字列を返す
	private String getRankStr(int nPoint) {
		String res = "BUST";
		if (2 <= nPoint && nPoint <= 20) {
			res = String.format("R%d", nPoint);
		} else if (nPoint == 21) {
			res = "BJ";
		} else {
			res = "BUST";
		}

		return res;
	}

}
