package sample;

public class BlackJack extends CardGame implements ICardGame {

	final static int HAND_CARD_NUMBER = 2;
	static String resultFile = "data\\BlackJack_result.txt";

	public String getResultFile() {
		return resultFile;
	}

	public int getHandCount() {
		return HAND_CARD_NUMBER;
	}

	/**
	 *
	 * @param currentHand
	 * @return
	 */
	public String getRank(String currentHand) {
		try {
			errorCheck(currentHand, HAND_CARD_NUMBER);
		} catch (Exception e) {
			super.logger.warning(e.getMessage() + ":" + currentHand);
			return e.getMessage();
		}

		// 点数判定
		int nPoint = getPoint(currentHand);

		// 結果文字列作成
		String result = getRankStr(nPoint);

		return result;
	}

	//	public String getRank(String currentHand) {
	//
	//		System.out.println(currentHand);
	//
	//
	//		String[] input = currentHand.split(",", -1);// 配列にする
	//
	//		int[] arrayCardNumber = new int[HAND_CARD_NUMBER];
	//		arrayCardNumber = super.getArrayOnlyCardNumber(input);
	//
	//		int sumNumber = sumNumber(arrayCardNumber);// 片方が１の場合の合計考える
	//													// 数字の合計を考える
	//		String result = checkBlackJack(sumNumber);// 21なのかを判定
	//
	//		return result;
	//
	//	}

	public int checkChangeNum(String yesNo) {
		return 1;
	};

	public String changHand(String playerHand, String yesNo, String newCard) {
		String result = playerHand + "," + newCard;
		return result;
	};

	public String checkWinner(String playerResult, String dealerResult) {
		// 同役は負け
		if (playerResult.equals(dealerResult)) {
			return "lose";
		}
		// ディーラーBJは負け
		if (dealerResult.equals("BJ")) {
			return "lose";
		}
		// プレーヤーがブタは負け
		if (playerResult.equals("BUST")) {
			return "lose";
		}
		// ディーラーが同役以外でBUSTは勝ち
		if (dealerResult.equals("BUST")) {
			return "win";
		}
		// プレーヤーが同役以外でBJは勝ち
		if (playerResult.equals("BJ")) {
			return "win";
		}
		// 数値判定でディーラーが上は負け
		int pR = Integer.parseInt(playerResult.replace("R", ""));
		int dR = Integer.parseInt(dealerResult.replace("R", ""));
		if (pR < dR) {
			return "lose";
		}
		return "win";
	};

	public boolean checkRequest(String request) {
		return false;
	};

	private void errorCheck(String currentHand, int HAND_CARD_NUMBER) throws Exception {
		String[] input = currentHand.split(",", -1);

		super.inputValueCheck(input, HAND_CARD_NUMBER);
		super.eachHandNum(input);
		super.ｃorrectCheckSuit(input);
		super.checkline(input);
		super.ｃorrectCheckNumber(input);
		super.checkSameCard(input);
	}

	//	private int countNumber(int cardNumber) {
	//		int num = cardNumber;
	//		if (cardNumber >= 10) {
	//			num = 10;
	//			return num;
	//		}
	//		return num;
	//	}

	// private int count_1_Number(int cardNumber) {
	// int num = cardNumber;
	//
	// if (cardNumber == 1) {
	// num = 11;
	// return num;
	// }
	// return num;
	// }

	//	private int sumNumber(int[] arrayCardNumber) {
	//		int num;
	//
	//		if (arrayCardNumber[0] == 1 && arrayCardNumber[1] == 1) {
	//			return 12;
	//		} else if (arrayCardNumber[0] == 1 && !(arrayCardNumber[1] == 1)) {
	//			num = 11 + countNumber(arrayCardNumber[1]);
	//			return num;
	//		} else if (!(arrayCardNumber[0] == 1) && arrayCardNumber[1] == 1) {
	//			num = countNumber(arrayCardNumber[0]) + 11;
	//			return num;
	//		} else {
	//			num = countNumber(arrayCardNumber[0]) + countNumber(arrayCardNumber[1]);
	//			return num;
	//		}
	//	}

	//	private static String checkBlackJack(int sumNumber) {
	//		if (sumNumber != 21) {
	//			return "R" + String.valueOf(sumNumber);
	//		} else {
	//			return "BJ";
	//		}
	//	}

	// BJの合計点を返す
	private int getPoint(String hand) {
		// カード分割
		String[] cards = hand.split(",", 0);

		int total = 0;
		// カードの枚数分ループする
		int exist1 = 0;
		for (int i = 0; i < cards.length; i++) {
			// カード分析
			int p = Integer.parseInt(cards[i].substring(1, cards[i].length()));
			// 点数計算
			if (p == 1) {
				exist1++;
				total += 1;
			} else if (p <= 10) {
				total += p;
			} else {
				total += 10;
			}
		}
		if (total <= 11 && exist1 > 0) {
			total += 10;
		}

		// 合計点を返す
		return total;
	}

	// 合計点に対応する文字列を返す
	private String getRankStr(int nPoint) {
		String res = "BUST";
		if (2 <= nPoint && nPoint <= 20) {
			res = String.format("R%d", nPoint);
		} else if (nPoint == 21) {
			res = "BJ";
		} else {
			res = "BUST";
		}

		return res;
	}
}
// クラス名
// BlackJack
//
// カードを2枚のみ使用（本当は違うけど）
//
// 役
// 1は1が2枚の場合を除いて、11点とする
// 11,12,13は10点とする
// 他は数字通りの点
//
// BJ：点数が21の場合
// Rn（nは2～20）：BJ以外の場合
//
// 例）H1,H11→ 11+10 → 21 → BJ
// D1,N8→ 11+8 → 19 → R19
// D1,H1→ 1+1 → 2 → R2
//
//
// 役を出力するプログラムを作成
