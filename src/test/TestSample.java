package test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import sample.BlackJack;
import sample.Poker;

public class TestSample {
	// テストカテゴリー番号
	static int category = 0;
	// テスト番号
	static int no = 1;

	public static void main(String[] args) {
		// Pokerクラスの確認
		newCategory();
		Poker pk = new Poker();

		// getHandCount
		outResult(pk.getHandCount() == 5);

		// 左は渡すカード情報、右は期待する返値
		String testPokerClassStr[][] = {
				// 正常系
				{ "H1,H2,D9,D3,D5", "NP" }, { "H1,H2,D1,D3,D5", "1P" }, { "H1,H2,D1,D3,D2", "2P" },
				{ "H1,S2,D2,C2,D5", "3C" }, { "H9,H10,D11,D12,D13", "ST" }, { "S1,S11,S12,S13,S9", "FL" },
				{ "H1,H3,D1,S3,C3", "FH" }, { "H13,D13,C13,S13,D5", "4C" }, { "D7,D6,D5,D4,D3", "STFL" },
				{ "C10,C11,C12,C13,C1", "RSTFL" },
				// 異常系
				{ "D7,D6,D5,D4", "inputValueError" }, { "C10,C11,C12,C13,C1,C3", "inputValueError" },
				{ "D7,D7,D7,D7,D7", "SameCardError" }, { "10,C11,C12,C13,C1,C3", "inputValueError" },
				{ "H0,H2,D9,D3,D5", "numberError" }, { "H14,H2,D1,D3,D5", "numberError" },
				{ "H1.H2,D9,D3,D5", "inputValueError" }, { "H14,D5,D1,D3,", "inputValueError" },
				{ ",,,,", "inputValueError" }, { "H14444444444", "inputValueError" },
				{ "F1,H2,D9,D3,D5", "suitError" }, };
		for (String[] testIte : testPokerClassStr) {
			String res = pk.getRank(testIte[0]);
			System.out.println(testIte[0]);
			System.out.println(res);
			outResult((res).equals(testIte[1]));
		}

		// BlackJackクラスの確認
		newCategory();
		BlackJack bj = new BlackJack();

		// getHandCount
		outResult(bj.getHandCount() == 2);

		// 左は渡すカード情報、右は期待する返値
		String testBlackJackClassStr[][] = {
				// 正常系
				{ "H1,D12", "BJ" }, { "C13,S1", "BJ" }, { "D10,D1", "BJ" }, { "D2,S5", "R7" }, { "D1,H1", "R12" },
				{ "S7,C9", "R16" }, { "H9,C9", "R18" }, { "H2,H3", "R5" }, { "H12,D10", "R20" }, { "C5,C13", "R15" },
				// 異常系
				{ "D5,H7,C12", "inputValueError" }, { "S4,H1,C6,C13", "inputValueError" }, { "H1,H1", "SameCardError" },
				{ "H75836523", "inputValueError" }, { "H14,D0", "numberError" }, { "D2,D190", "inputValueError" },
				{ "83", "inputValueError" }, { "", "inputValueError" }, { ",", "inputValueError" },
				{ "A7,O45", "suitError" }, { "H12,K5", "suitError" }, };
		for (String[] testIte : testBlackJackClassStr) {
			String res = bj.getRank(testIte[0]);
			System.out.println(testIte[0]);
			System.out.println(res);
			outResult((res).equals(testIte[1]));
		}
	}

	private static void newCategory() {
		category++;
		no = 1;
	}

	private static void outResult(boolean b) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Tokyo"));
		System.out.println(String.format("%s <<%2d - %4d>> : %s", sdf.format(date), category, no, (b ? "OK" : "NG")));
		no++;
	}
}
