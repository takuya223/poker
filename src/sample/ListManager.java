package sample;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ListManager {

	private File file;
	private ArrayList<String> arrayResult;

	ListManager(String name) {
		this.file = new File(name);

	}

	public void resultWrite(String result) {
		Date now = new Date();
		SimpleDateFormat sdF = new SimpleDateFormat("YYYY/MM/dd HH:mm:ss");
		String nowTime = sdF.format(now);
		try {
			// File file = new
			// File("c:\\Users\\tky50\\workspace\\sample\\data\\result.txt");

			if (checkBeforefile(this.file)) {
				FileWriter filewriter = new FileWriter(this.file, true);

				filewriter.write(nowTime);
				filewriter.write(" ");
				filewriter.write(result);
				filewriter.write("\r\n");

				filewriter.close();
			} else {
				System.out.println("ファイルに書き込めません");
			}
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	public void readResult() {
		try {
			// File file = new
			// File("c:\\Users\\tky50\\workspace\\sample\\data\\result.txt");

			if (checkBeforefile(this.file)) {
				BufferedReader br = new BufferedReader(new FileReader(this.file));

				String str;
				while ((str = br.readLine()) != null) {
					System.out.println(str);
				}

				br.close();
			} else {
				System.out.println("ファイルが見つからないか開けません");
			}
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	public void arrayResult() {
		this.arrayResult = new ArrayList<String>();
		try {
			// File file = new
			// File("c:\\Users\\tky50\\workspace\\sample\\data\\result.txt");

			BufferedReader br = new BufferedReader(new FileReader(this.file));

			String str;
			while ((str = br.readLine()) != null) {
				arrayResult.add(str);
			}

			br.close();
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}

	}

	private static boolean checkBeforefile(File file) throws IOException {
		if (file.exists()) {
			if (file.isFile() && file.canWrite()) {
				return true;
			}

		} else {
			file.createNewFile();
			return true;
		}

		return false;
	}

}