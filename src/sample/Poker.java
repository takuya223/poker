package sample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * ポーカーゲームの勝敗を決定するために、必要な判定を行うクラス
 * 
 * @author tky50
 *
 */
public class Poker extends CardGame implements ICardGame {

	final static String resultFile = "data\\Poker_result.txt";

	final static int MAX_CARD_NUMBER = 13;

	final static int HAND_CARD_NUMBER = 5;

	final String[] rank = { "RSTFL", "STFL", "4C", "FH", "FL", "ST", "3C", "2P", "1P", "NP" };

	public String getResultFile() {
		return resultFile;
	}

	public int getHandCount() {
		return HAND_CARD_NUMBER;
	}

	public String getRank(String currentHand) {
		// System.out.println(currentHand);

		String[] handrank = { "rstfl", "stfl", "4c", "fh", "fl", "st", "3c", "2p", "1p", "np" };

		try {
			errorCheck(currentHand, HAND_CARD_NUMBER);
		} catch (Exception e) {
			super.logger.warning(e.getMessage() + ":" + currentHand);
			return e.getMessage();
		}
		String[] input = currentHand.split(",", 0);// 入力値を持つてくる

		int[] arrayCardNumber = new int[HAND_CARD_NUMBER];
		arrayCardNumber = super.getArrayOnlyCardNumber(input);

		// 「カードを判別していく」
		String[] arraySuit = new String[HAND_CARD_NUMBER];// アルファベットのみを取り出してスートを判断
		arraySuit = getArraySuit(input);

		int[] numberOfCard = new int[MAX_CARD_NUMBER];// 各数字の枚数
		numberOfCard = getNumberOfCard(arrayCardNumber);

		handrank[4] = check_FL(arraySuit);// FLを見分ける関数
		handrank[5] = check_Straight(arrayCardNumber);// STを見分ける関数を作る
		handrank[6] = check_3Card(numberOfCard);// 3Cを判断する関数
		handrank[7] = check_2Pair(numberOfCard);// 2Pを判断する関数
		handrank[8] = check_Pair(numberOfCard);// 1Pを判断する関数
		handrank[9] = check_NoPair(numberOfCard);// NPを判断する関数
		handrank[2] = check_4Card(numberOfCard);// 4Cを判断する関数
		handrank[3] = check_FH(handrank);// FHを判断する関数
		handrank[1] = check_STFL(handrank);// STFLを判断する関数
		handrank[0] = check_RSTFL(handrank, arrayCardNumber); // RSTFLを判断する関数

		String StrongestHandRank = checkStrongestHandRank(rank, handrank);// 手札の中で最も強い役を持ってくる
		return StrongestHandRank;
	}

	public int checkChangeNum(String yesNo) {
		String[] punctuateYesNo = yesNo.split(",");
		return punctuateYesNo.length;

	}

	public String changHand(String playerHand, String yesNo, String newCard) {

		String[] arrayYesNo = yesNo.split(",");
		String[] arrayNewCard = newCard.split(",");
		String[] arrayPlayerHand = playerHand.split(",");

		for (int j = 0; j < arrayYesNo.length; j++) {
			for (int i = 0; i < arrayPlayerHand.length; i++) {

				if (arrayYesNo[j].equals(arrayPlayerHand[i])) {
					arrayPlayerHand[i] = arrayNewCard[j];
				}

			}
		}
		String result = String.join(",", arrayPlayerHand);
		return result;
	}

	/*
	 * (非 Javadoc)
	 * 
	 * @see sample.ICardGame#checkWinner(java.lang.String, java.lang.String)
	 */
	public String checkWinner(String playerResult, String dealerResult) {
		int playerPoint = checkPoint(playerResult);
		int dealerPoint = checkPoint(dealerResult);
		String[] res = new String[2];
		try {
			// 先程インストールしたMySQLのドライバを指定
			Class.forName("com.mysql.jdbc.Driver");

			// MySQLデータベースに接続 (DB名,ID,パスワードを指定)
			Connection conn = DriverManager
					.getConnection("jdbc:mysql://192.168.100.3/test?" + "user=root&password=guruguru");
			// ステートメントを作成
			Statement stmt = conn.createStatement();

			String lang = "ja";
			// SELECT
			ResultSet rset = stmt.executeQuery("SELECT m.message from message m where lang='" + lang + "' order by id");
			int i = 0;
			while (rset.next()) {
				res[i] = rset.getString(1); // ()内は列番号です
				i++;
			}

			// 結果セットをクローズ
			rset.close();
			// ステートメントをクローズ
			stmt.close();
			// 接続をクローズ
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (playerPoint < dealerPoint) {
			return res[0];
		} else {
			return res[1];
		}

	}

	/*
	 * (非 Javadoc)
	 * 
	 * @see sample.ICardGame#checkRequest(java.lang.String)
	 */
	public boolean checkRequest(String request) {

		try {
			checkRequestError(request);

		} catch (Exception e) {
			// super.logger.warning(e.getMessage() + ":" + request);
			// System.err.println(e.getMessage());
			return true;
		}
		return false;

	}

	// プレイヤーと自分のカードを比べて、強い方を判断する
	/**
	 * @param Result
	 *            決定したポーカーの役
	 * @return 役をフィールドにあるrankの強い順に比べていき、
	 */
	private int checkPoint(String Result) {
		int point = 0;
		for (int i = 0; i < this.rank.length; i++) {
			if (Result.equals(this.rank[i]))
				point = i;
		}
		return point;
	}

	// カードを交換する際の入力値が不正でないか判断する

	/**
	 * @param request
	 * @throws Exception
	 */
	private void checkRequestError(String request) throws Exception {
		String[] arrayrequest = request.split(",", -1);

		// super.checkExistCard(arrayrequest);
		super.eachHandNum(arrayrequest);
		super.ｃorrectCheckSuit(arrayrequest);
		super.checkline(arrayrequest);
		super.ｃorrectCheckNumber(arrayrequest);
		super.checkSameCard(arrayrequest);
	}

	// 始めの5枚のカードに不正がないか判断する
	/**
	 * @param currentHand
	 * @param HAND_CARD_NUMBER
	 * @throws Exception
	 */
	private void errorCheck(String currentHand, int HAND_CARD_NUMBER) throws Exception {
		String[] input = currentHand.split(",", -1);

		super.inputValueCheck(input, HAND_CARD_NUMBER);
		super.eachHandNum(input);
		super.ｃorrectCheckSuit(input);
		super.checkline(input);
		super.ｃorrectCheckNumber(input);
		super.checkSameCard(input);
	}

	/**
	 * @param input
	 * @return
	 */
	private static String[] getArraySuit(String input[]) {
		String[] suit = new String[HAND_CARD_NUMBER];// アルファベットのみを取り出してスートを判断
		for (int i = 0; i < suit.length; i++) {
			suit[i] = input[i].substring(0, 1);
		}
		return suit;
	}

	/**
	 * @param suit
	 * @return
	 */
	private static String check_FL(String[] suit) { // FLを見分ける関数

		for (int i = 0; i < 4; i++) {
			if (!(suit[i].equals(suit[i + 1]))) {
				return "fl";
			}
		}
		return "FL";
	}

	/**
	 * @param num
	 * @return
	 */
	private static int[] getNumberOfCard(int[] num) {// 各数字の枚数
		// System.out.println(num.length);
		int[] number = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
		int[] arrayCountNumber = new int[MAX_CARD_NUMBER];
		for (int i = 0; i < number.length; i++) {
			for (int n = 0; n < num.length; n++) {
				if (number[i] == num[n]) {
					arrayCountNumber[i]++;
				}
			}
		}
		return arrayCountNumber;
	}

	private static String check_4Card(int[] numberOfCard) {// 4Cを判断

		for (int i = 0; i < numberOfCard.length; i++) {
			if (numberOfCard[i] == 4) {
				return "4C";
			}
		}
		return "4c";
	}

	private static String check_3Card(int[] numberOfCard) {// 3Cを判断
		for (int i = 0; i < numberOfCard.length; i++) {
			if (numberOfCard[i] == 3) {
				return "3C";

			}
		}
		return "3c";
	}

	private static String check_2Pair(int[] numberOfCard) {// 2Pを判断
		int p = 0;
		for (int i = 0; i < numberOfCard.length; i++) {
			if (numberOfCard[i] == 2) {
				p++;
			}
		}
		if (p == 2) {
			return "2P";
		}
		return "2p";
	}

	private static String check_Pair(int[] numberOfCard) {// 1Pを判断
		int p = 0;
		for (int i = 0; i < numberOfCard.length; i++) {
			if (numberOfCard[i] == 2) {
				p++;
			}
		}
		if (p == 1) {
			return "1P";

		}
		return "1p";
	}

	private static String check_NoPair(int[] numberOfCard) {// NPを判断
		int p = 0;
		for (int i = 0; i < numberOfCard.length; i++) {
			if (numberOfCard[i] == 2) {
				p++;
			}
		}
		if (p < 1) {
			return "NP";
		}
		return "np";
	}

	private static int[] descendingSort(int[] arrayCardNumber) {// 降順に並び替える

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4 - i; j++) {
				if (arrayCardNumber[j] < arrayCardNumber[j + 1]) {
					int temp = arrayCardNumber[j];
					arrayCardNumber[j] = arrayCardNumber[j + 1];
					arrayCardNumber[j + 1] = temp;
				}
			}
		}
		return arrayCardNumber;
	}

	private static String check_Straight(int[] arrayCardNumber) {// STを判断
		int[] newNumLine = descendingSort(arrayCardNumber);

		if (newNumLine[0] == 13 && newNumLine[1] == 12 && newNumLine[2] == 11 && newNumLine[3] == 10
				&& newNumLine[4] == 1) {// {1,13,12,11,10}の場合の判断
			return "ST";
		}
		for (int i = 0; i < newNumLine.length - 1; i++) {// {1,13,12,11,10}以外のSTの判断
			if (!(newNumLine[i] == newNumLine[i + 1] + 1)) {
				return "st";
			}
		}
		return "ST";
	}

	private String check_FH(String[] handrank) { // FHを判断
		if (handrank[6].equals(this.rank[6]) && handrank[8].equals("1P")) {
			return "FH";
		}
		return "fh";
	}

	private static String check_STFL(String[] handrank) { // STFLを判断
		if (handrank[4].equals("FL") && handrank[5].equals("ST")) {
			return "STFL";
		}
		return "stfl";
	}

	private static String check_RSTFL(String[] handrank, int[] arrayCardNumber) {// RSTFLを判断する関数
		int[] newNumLine = descendingSort(arrayCardNumber);
		if (handrank[4].equals("FL") && handrank[5].equals("ST") && newNumLine[0] == 13
				&& newNumLine[newNumLine.length - 1] == 1) {
			return "RSTFL";
		}
		return "rstfl";

	}

	/**
	 * @param rank
	 * @param handrank
	 * @return
	 */
	private static String checkStrongestHandRank(String[] rank, String[] handrank) { // 手札で一番強い役を求める
		for (int i = 0; i < rank.length; i++) {
			for (int j = 0; j < handrank.length; j++) {
				if (rank[i].equals(handrank[j])) {
					return handrank[j];
				}
			}
		}
		return "error";
	}

}
