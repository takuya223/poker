package sample;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * @author tky50
 *
 */
public class CardGame {

	// protected static final String LOGGING_PROPERTIES = "javalog.properties";
	protected Logger logger;

	/**
	 * ロガーの設定をする
	 */
	CardGame() {
		this.logger = Logger.getLogger("SampleLogging");
		// クラスパスの中から ログ設定プロパティファイルを取得
		// logger.fine("ログ設定: " + LOGGING_PROPERTIES + " をもとにログを設定します。");
		final InputStream inStream = CardGame.class.getClassLoader().getResourceAsStream("javalog.properties");
		if (inStream == null) {
			logger.info("ログ設定: " + "javalog.properties" + " はクラスパス上に見つかりませんでした。");
		} else {
			try {
				LogManager.getLogManager().readConfiguration(inStream);
				// logger.config("ログ設定: LogManagerを設定しました。");
			} catch (IOException e) {
				logger.warning("ログ設定: LogManager設定の際に" + "例外が発生しました。:" + e.toString());
			} finally {
				try {
					if (inStream != null)
						inStream.close();
				} catch (IOException e) {
					logger.warning("ログ設定: ログ設定プロパティ" + "ファイルのストリームクローズ時に例外が" + "発生しました。:" + e.toString());
				}
			}
		}
	}

	protected int[] getArrayOnlyCardNumber(String[] input) {//
		// 2文字目以降を取り出し配列にする
		int[] newnum = new int[input.length];
		for (int i = 0; i < newnum.length; i++) {
			if (input[i].length() == 2) {

				newnum[i] = Integer.parseInt(input[i].substring(1, 2));
			} else if (input[i].length() == 3) {
				newnum[i] = Integer.parseInt(input[i].substring(1, 3));
			}

		}
		return newnum;

	}

	protected void inputValueCheck(String[] input, int HAND_CARD_NUMBER) throws Exception {
		if (input.length == HAND_CARD_NUMBER) {
			// return "correct";
		} else {
			throw new Exception("inputValueError");
			// return "inputValueError";
		}

	}

	protected void eachHandNum(String[] input) throws Exception {
		for (int i = 0; i < input.length; i++) {
			if (input[i].length() < 1 || 3 < input[i].length()) {
				// return "inputValueError";
				throw new Exception("inputValueError");
			}
		}
		// return "correct";
	}

	protected String ｃorrectCheckSuit(String[] input) throws Exception { // スートのアルファベットが正しいか判断する関数
		String[] suit = new String[input.length];// アルファベットのみを取り出す
		for (int i = 0; i < suit.length; i++) {
			suit[i] = input[i].substring(0, 1);
		}
		// return suit;
		for (int i = 0; i < suit.length; i++) {
			if (!(suit[i].equals("H") || suit[i].equals("D") || suit[i].equals("C") || suit[i].equals("S")
					|| suit[i].equals("J"))) {
				throw new Exception("suitError");
			}
		}
		return "correct";
	}

	protected void checkline(String[] input) throws Exception {
		String[] newnum = new String[input.length];
		for (int i = 0; i < newnum.length; i++) {
			if (input[i].length() == 2) {
				newnum[i] = input[i].substring(1, 2);
			} else if (input[i].length() == 3) {
				newnum[i] = input[i].substring(1, 3);
			}
		}
		for (int i = 0; i < newnum.length; i++) {// 二文字目以降は数字かどうかの判断
			try {
				Integer.parseInt(newnum[i]);
				// return "correct";
			} catch (NumberFormatException e) {
				// return "NumberFormatException";
				throw new NumberFormatException("NumberFormatException");
			}

		}

	}

	protected void ｃorrectCheckNumber(String[] input) throws Exception {
		int[] arrayCardNumber = getArrayOnlyCardNumber(input);

		for (int i = 0; i < arrayCardNumber.length; i++) {
			if (!(1 <= arrayCardNumber[i] && arrayCardNumber[i] <= 13)) {
				throw new Exception("numberError");
			}
		}
	}

	protected void checkSameCard(String[] input) throws Exception {

		for (int i = 0; i < input.length - 1; i++) {
			for (int j = 0; j < input.length; j++) {

				if (i != j) {

					if (input[i].equals(input[j])) {
						throw new Exception("SameCardError");
					}
				}
			}
		}

	}

}
