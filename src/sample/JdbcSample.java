package sample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

class JdbcSample {
	public static void main(String[] args) {
		try {
			// 先程インストールしたMySQLのドライバを指定
			Class.forName("com.mysql.jdbc.Driver");

			// MySQLデータベースに接続 (DB名,ID,パスワードを指定)
			Connection conn = DriverManager
					.getConnection("jdbc:mysql://192.168.100.3/test?" + "user=root&password=guruguru");
			// ステートメントを作成
			Statement stmt = conn.createStatement();

			// INSERT
			// stmt.executeUpdate("INSERT INTO users (id, email) VALUES
			// (1001,'someone01@example.com')");
			// stmt.executeUpdate("INSERT INTO users (id, email) VALUES
			// (1002,'someone02@example.com')");
			//
			// // DELETE
			// stmt.executeUpdate("DELETE FROM users where id=1001");
			//
			// // UPDATE
			// stmt.executeUpdate("UPDATE users SET email='updated@example.com'
			// where id=1002");

			// SELECT
			ResultSet rset = stmt.executeQuery("SELECT * from message");

			while (rset.next()) {

				System.out.println(rset.getString(1) + rset.getString(2) + rset.getString(3) + rset.getString(4)); // ()内は列番号です

			}

			// 結果セットをクローズ
			rset.close();
			// ステートメントをクローズ
			stmt.close();
			// 接続をクローズ
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}