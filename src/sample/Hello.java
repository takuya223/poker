package sample;

import java.util.Scanner;

public class Hello {
	/**
	 * 
	 * 
	 * @param args
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		// 名前入力
		System.out.print("名前を入力して下さい：");
		String name = sc.nextLine();

		// ゲーム選択
		System.out.print("Poker -> 0 or\nBlackJack -> 1\n：");
		String game = sc.nextLine();

		ICardGame cg = null;
		if (game.equals("1")) {
			cg = new BlackJack();
		} else {
			cg = new Poker();
		}

		// カードの山初期化
		Card c = new Card();

		int gameCount = 0;
		int winCount = 0;
		int loseCount = 0;
		while (true) {
			gameCount++;

			System.out.println("ゲーム　第" + gameCount + "回");

			String dealerHand = c.getRundumCsvStr(cg.getHandCount());
			// ディーラーの役決定
			String dealerResult = cg.getRank(dealerHand);

			// プレーヤーの初手
			String playerHand = c.getRundumCsvStr(cg.getHandCount());
			// ディーラーの役決定
			String playerResult = cg.getRank(playerHand);

			// sc.close();
			System.out.println("Dealer's Hand => " + dealerResult);

			while (true) {
				System.out.println(playerHand);
				System.out.println(playerResult);
				System.out.print("勝負しますか？Yes=>0  No(交換)=>(交換札をカンマ区切りで)：");

				String yesNo = sc.nextLine();
				if (yesNo.equals("0")) {
					// cg のメソッド呼び出しでディーラーとの勝敗を決定
					String winner = cg.checkWinner(playerResult, dealerResult);
					if (winner.equals("win")) {
						winCount++;
					} else {
						loseCount++;
					}
					System.out.println(winner);
					break;

				} else {
					System.out.println("手札交換");
					// cg のメソッド呼び出しで手札交換実施

					if (cg.checkRequest(yesNo)) {
						continue;
					}

					// 交換する枚数を判断
					int changeNum = cg.checkChangeNum(yesNo);

					// 新しい手札をもらう
					String newCard = c.getRundumCsvStr(changeNum);

					// playerHandの更新
					playerHand = cg.changHand(playerHand, yesNo, newCard);

					// playerResultの更新
					playerResult = cg.getRank(playerHand);

					// break;
				}

			}
			System.out.print("終了しますか？Yes=>0 ：");

			String nextGame = sc.nextLine();
			if (nextGame.equals("0")) {
				break;
			}
		}
		sc.close();

		ListManager lm = new ListManager(cg.getResultFile());

		String resultLog = String.format("ゲーム数：%3d 勝：%2d 負:" + "%2d プレーヤー：%s さん", gameCount, winCount, loseCount, name);
		lm.resultWrite(resultLog);

		System.out.println(name + "は屈強な男たちにかかえられて、店の奥へと消えていった\nその後、" + name + "を見たものはたれもいない");

	}

}