package sample;

public interface ICardGame {
	/**
	 * 配られるカードから役を判断する
	 * 
	 * @param currentHand
	 *            始めに配られるゲームの手札
	 * @return 手札から判断した役
	 */
	public String getRank(String currentHand);

	/**
	 * 各ゲームで使うカードの枚数を返す
	 * 
	 * @return 各ゲームで使うカードの枚数
	 */
	public int getHandCount();

	/**
	 * 各ゲームの結果を蓄積するファイルのパスを返す
	 * 
	 * @return ゲーム結果ファイルのパス
	 */
	public String getResultFile();

	/**
	 * 変更するカードの枚数を返す
	 * 
	 * @param yesNo
	 *            変更対象
	 * @return 変更するカードの枚数
	 */
	public int checkChangeNum(String yesNo);

	/**
	 * 手持ちのカードを変更する
	 * 
	 * @param playerHand
	 *            プレイヤーの手持ちのカード
	 * @param yesNo
	 *            変更対象
	 * @param newCard
	 *            新しく配られるカード
	 * @return カードを変更した手札
	 */
	public String changHand(String playerHand, String yesNo, String newCard);

	/**
	 * プレイヤーとディーラーの役を比べて、プレイヤーの勝敗を判断する
	 * 
	 * @param playerResult
	 *            プレイヤーの役
	 * @param dealerResult
	 *            ディーラーの役
	 * @returnプレイヤーの勝敗
	 */
	public String checkWinner(String playerResult, String dealerResult);

	/**
	 * 変更対象の正否を確かめる
	 * 
	 * @param request
	 *            変更対象
	 * @return 変更対象の正否
	 */
	public boolean checkRequest(String request);
}
